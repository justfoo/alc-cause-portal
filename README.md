# Install Notes

* Hyper-V Manager
* "Quick Create..." 
  * https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/quick-create-virtual-machine
  * Local installation source (Ubuntu Server 18.04.2 LTS)
  * ubuntu-18.04.2-live-server-amd64.iso
  * Uncheck "This virual machine will run Windows"
  * Provide a Virtual Machine Name
    * HERMES -- Ubuntu Server 18.04.1 LTS
* Connect to the new machine (https://www.howtoforge.com/tutorial/ubuntu-lts-minimal-server/)
  * Run through install options
    * Default options are mostly fine
    * establish your own username and password
    * Make sure to install OpenSSH to allow remote terminal login
    * reboot when complete

# Post-install configuration

```sh
# Login Remotely with the user credentials you established in the install
# Ensure your install is up-to-date

sudo apt-get update
sudo apt-get -y upgrade

# Loosely based on instructions here: https://www.digitalocean.com/community/tutorials/how-to-set-up-an-apache-mysql-and-python-lamp-server-without-frameworks-on-ubuntu-14-04
# Install and configure LAMP components
# MySQL strictly not needed for this project, but is a standard component for most web hosting

sudo apt-get -y install apache2 mysql-server python3 python3-pip python3-django

# Ensure MySQL is locked down

sudo -i mysql_secure_installation
#set MySQL root password

# Configure Python and install needed components

# set python3 as the default
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1
#make a symlink so 'pip' pints to 'pip3'
sudo ln -s /usr/bin/pip3 /usr/bin/pip

sudo -H pip install spacy pymysql beautifulsoup4 pandas lxml cssutils xlrd openpyxl datefinder

#Get the standard SpaCy english-language models:
sudo -H python3 -m spacy download en_core_web_sm
sudo -H python3 -m spacy download en_core_web_md
sudo -H python3 -m spacy download en_core_web_lg
sudo -H python3 -m spacy download en_vectors_web_lg


#Make a dedicated directory for this project
sudo mkdir -p /var/www/uw-causes
sudo chown $USER:$USER /var/www/uw-causes
ln -s /var/www/uw-causes ~/uw-causes

# insert some apache directives into the default configuration file
head -n 1 /etc/apache2/sites-enabled/000-default.conf > ~/000-default.conf.new
echo -e "
    <Directory /var/www/uw-causes>
        Options +ExecCGI
        DirectoryIndex index.py
    </Directory>
    AddHandler cgi-script .py
" >> ~/000-default.conf.new
tail -n +2 /etc/apache2/sites-enabled/000-default.conf >> ~/000-default.conf.new
sed -i 's/\/var\/www\/html/\/var\/www\/uw-causes/g' ~/000-default.conf.new
sudo mv ~/000-default.conf.new /etc/apache2/sites-enabled/000-default.conf
sudo chown root:root /etc/apache2/sites-enabled/000-default.conf


# disable multithreading processes.
sudo a2dismod mpm_event
# give Apache explicit permission to run scripts.
sudo a2enmod mpm_prefork cgi

sudo service apache2 restart


# Create basic python script for testing
echo -e '#!/usr/bin/python3
# Print necessary headers.
print("Content-Type: text/html")
print()
print("Hello World!")' > /var/www/uw-causes/hello_world.py

chmod 755 /var/www/uw-causes/hello_world.py
```

