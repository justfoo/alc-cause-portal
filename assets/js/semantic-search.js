

//Event Listeners
document.getElementById('searchData').addEventListener('submit', searchDataSubmit);
document.getElementById('searchData').addEventListener('reset',  searchDataReset);

//Event Handler for submitting the form
function searchDataSubmit(event){
    event.preventDefault();

    var text = document.getElementById('searchTextarea').value;
    var renderedText = text;

    //values to pass to the API call
    const options = {
        method: 'POST',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        credentials: 'same-origin',
        body: JSON.stringify({ text, model })
    };

    //Remove any existing data
    document.getElementById('searchResults').innerHTML = '';

    //Makes API call to get entity info for supplied text.
    fetch(endpoint + '/sim', options)
        .then(response => {
            return response.json()
        })
        .then(data => {

            // Work with JSON data here
            // Extract the token that each item refers to
            // future improvement - dynamically extract the categories
            // allowing the scraper to make arbitrary assignments
            // because this is really really lame.
            renderedText = "";
            renderedEvent = "";
            renderedGiftFund = "";
            renderedKUOWContent = "";
            renderedMagazineArticle = "";
            renderedNewsArticle = "";
            renderedOther = "";
            for (var i = 0; i < data.length; i++) {
                var textBlob=''
                var category = data[i]['Category'];
                var score = (Number(data[i]['Score']) * 100).toFixed() + "%";
                var title = data[i]['Title'];
                var url = data[i]['URL'];
                textBlob = '<li><span class="scoreVal">'+ score +'</span> <a href="'+url+'">'+ title + '</a></li>';

                switch(category) {
                    case 'Event':
                        renderedEvent = renderedEvent + textBlob;
                        break;
                    case 'Gift Fund':
                        renderedGiftFund = renderedGiftFund + textBlob;
                        break;
                    case 'KUOW Content':
                        renderedKUOWContent = renderedKUOWContent + textBlob;
                        break;
                    case 'Magazine Article':
                        renderedMagazineArticle = renderedMagazineArticle + textBlob;
                        break;
                    case 'News Article':
                        renderedNewsArticle = renderedNewsArticle + textBlob;
                        break;
                    default:
                        renderedOther = renderedOther + textblob;
                }
            }
            if(renderedGiftFund!="")        { renderedText = renderedText + '<h2>Gift Funds</h2><ul>'        + renderedGiftFund        +'</ul>'; }
            if(renderedKUOWContent!="")     { renderedText = renderedText + '<h2>KUOW Content</h2><ul>'      + renderedKUOWContent     +'</ul>'; }
            if(renderedMagazineArticle!="") { renderedText = renderedText + '<h2>Magazine Articles</h2><ul>' + renderedMagazineArticle +'</ul>'; }
            if(renderedNewsArticle!="")     { renderedText = renderedText + '<h2>News Articles</h2><ul>'     + renderedNewsArticle     +'</ul>'; }
            if(renderedEvent!="")           { renderedText = renderedText + '<h2>Events</h2><ul>'            + renderedEvent           +'</ul>'; }
            if(renderedOther!="")           { renderedText = renderedText + '<h2>Other</h2><ul>'             + renderedOther           +'</ul>'; }

            //Write data back to the page
            document.getElementById('searchResults').innerHTML = renderedText;

            //console.log(data);
        })

        .catch(err => {
            // Do something for an error here
            document.getElementById('searchResults').innerHTML = 'Oops! Something went wrong!';
        })

}

//Event Handler for resetting the form
function searchDataReset(event){
    event.preventDefault();
    document.getElementById('searchTextarea').value = '';
    document.getElementById('searchResults').innerHTML = '';
}

