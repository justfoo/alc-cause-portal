

//Event Listeners
document.getElementById('nerData').addEventListener('submit', nerDataSubmit);
document.getElementById('nerData').addEventListener('reset',  nerDataReset);

//Event Handler for submitting the form
function nerDataSubmit(event){
    event.preventDefault();

    var text = document.getElementById('nerTextarea').value;
    var renderedText = text;

    //values to pass to the API call
    const options = {
        method: 'POST',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        credentials: 'same-origin',
        body: JSON.stringify({ text, model })
    };

    //Remove any existing data
    document.getElementById('tokenCounts').innerHTML = '';
    document.getElementById('responseData').innerHTML = '';

    //Makes API call to get entity info for supplied text.
    fetch(endpoint + '/ent', options)
        .then(response => {
            return response.json()
        })
        .then(data => {

            // Work with JSON data here

            // Extract the token that each item refers to
            for (var i = 0; i < data.length; i++) {
                data[i]['token'] = renderedText.slice(data[i]['start'],data[i]['end']);
            }

            //walk the array in reverse
            for (var i = data.length - 1; i >= 0; --i) {

                //extract token from text string and set it in the array
                data[i]['token'] = renderedText.slice(data[i]['start'],data[i]['end']);

                if (data[i].label.match( tagBlacklistStr )  ||   /\n/.test(data[i].token)  ) {
                    //remove any entry where 1) token contains a newline character; or 2) label is in the blacklist
                    data.splice(i,1);
                } else {
                    //if not removed - we want it rendered
                    ////Synonyms
                    if (data[i].label == "PER") {label = "PERSON";}
                    if (data[i].label == "FACILITY") {label = "FAC";}
                    renderedText = renderedText.slice(0, data[i].end  ) + '</mark>' + renderedText.slice( data[i].end  );
                    renderedText = renderedText.slice(0, data[i].start) + '<mark data-entity="' + data[i].label + '">' + renderedText.slice( data[i].start );
                }
            }


            //All the possible tag types
            var allCategories = [
                { label: 'PERSON', descr: 'People, including fictional.'},
                { label: 'NORP', descr: 'Nationalities or religious or political groups.'},
                { label: 'FAC', descr: 'Buildings, airports, highways, bridges, etc.'},
                { label: 'ORG', descr: 'Companies, agencies, institutions, etc.'},
                { label: 'GPE', descr: 'Countries, cities, states.'},
                { label: 'LOC', descr: 'Non-GPE locations, mountain ranges, bodies of water.'},
                { label: 'PRODUCT', descr: 'Objects, vehicles, foods, etc.'},
                { label: 'EVENT', descr: 'Named hurricanes, battles, wars, sports events, etc.'},
                { label: 'WORK_OF_ART', descr: 'Titles of books, songs, etc.'},
                { label: 'LAW', descr: 'Named documents made into laws.'},
                { label: 'LANGUAGE', descr: 'Any named language.'},
                { label: 'DATE', descr: 'Absolute or relative dates or periods.'},
                { label: 'TIME', descr: 'Times smaller than a day.'},
                { label: 'PERCENT', descr: 'Percentage, including “%”.'},
                { label: 'MONEY', descr: 'Monetary values, including unit.'},
                { label: 'QUANTITY', descr: 'Measurements, as of weight or distance.'},
                { label: 'ORDINAL', descr: '“first”, “second”, etc.'},
                { label: 'CARDINAL', descr: 'Numerals that do not fall under another type.'},
            ];

            //Get a list of all the tag types returned
            //not using becuase we still want a legend of all ags, even if not in the provided text
            //var tagSurviving =  [...new Set( data.map(x => x.label) )];

            //remove blacklisted tags
            for (var i = allCategories.length - 1; i >= 0; --i) {
                if (allCategories[i].label.match( tagBlacklistStr )) {
                    //remove any entry where label is in the blacklist
                    allCategories.splice(i,1);
                }
            }

            //Walk the array and generate the HTML for the tags
            legendListText="";
            allCategories.forEach(function (arrayItem) {
                legendListText = legendListText + '<li><mark data-entity="' + arrayItem["label"] + '">' + arrayItem["descr"] + '</mark></li>';
                //console.log(arrayItem);
            });
            legendListText='<ul id="tagLegendList">' + legendListText + "</ul>";


            //Get counts for the number of times each token appears
            var tokenList = data.map(x => x.token);

            //Tally up the number of occurences for each token
            const tokenCounts = tokenList.reduce( (tally, tokenValue) => {
                tally[tokenValue] = (tally[tokenValue] || 0) + 1 ;
                return tally;
            } , {})

            //Create a container to hold the tokes sorted by counts.
            //note that it isn't yet sorted - just storing it in array to sort in the next step
            var tokenCountsSorted = [];
            for (var token in tokenCounts) {
                tokenCountsSorted.push([token, tokenCounts[token]]);
            }

            // Here's where the actual sorting occurs
            tokenCountsSorted.sort(function(a, b) {
                if (a[1] < b[1]) return 1;
                if (a[1] > b[1]) return -1;
                if (a[0] > b[0]) return 1;
                if (a[0] < b[0]) return -1;
            });


            //initialize the text containing the tags
            var tagListText = "";


            //Walk the array and generate the HTML for the tags
            tokenCountsSorted.forEach(function (arrayItem) {
                tagListText = tagListText + '<mark style="background: #bbb;" data-entity="'+arrayItem[1]+'">' + arrayItem[0] + '</mark>';
            });

            //if no tags, tell that to the user
            if (tagListText.length == 0) {
                tagListText = "No tags detected.";
            }

            //Write data back to the page
            document.getElementById('tokenCounts').innerHTML = "<h2>Tags</h2>"+tagListText;
            renderedText = "<p>"+renderedText.replace(/\r\n?|\n/g, '</p><p>')+"</p>"    //need to understand newlines/carriage returns better before this will work
            document.getElementById('responseData').innerHTML = "<h2>Content Text</h2>"+renderedText;
            document.getElementById('tagLegend').innerHTML = "<h3>Tag Legend</h3>"+legendListText;


            //console.log(data);
            //console.log(tokenList);
            //console.log(tokenCounts);
            //console.log(tokenCountsSorted);
        })

        .catch(err => {
            // Do something for an error here
            document.getElementById('responseData').innerHTML = 'Oops! Something went wrong!';
        })

}

//Event Handler for resetting the form
function nerDataReset(event){
    event.preventDefault();
    document.getElementById('nerTextarea').value = '';
    document.getElementById('tokenCounts').innerHTML = '';
    document.getElementById('responseData').innerHTML = '';
    document.getElementById('tagLegend').innerHTML = '';
}

