################################################################################################
# Post-install configuration
################################################################################################
#
# Ubuntu 18.04 LTS server
#
################################################################################################


################################################################################
###    Immediate Action to update and lockdown the box
################################################################################

    # add self to the sudoers file for passwordless elevation
#echo -e "$USER ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers

    # Setup firewall to only allow on specific ports

    # restrictive default policy
sudo ufw default allow outgoing
sudo ufw default deny incoming

    # Allow desired ports
sudo ufw allow 22/tcp comment 'Allow SSH traffic'
sudo ufw allow 80/tcp comment 'Allow HTTP traffic'
sudo ufw allow 443/tcp comment 'Allow HTTPS traffic'

    # turn it on
sudo ufw --force enable


    # Ensure the  install is up-to-date
sudo apt update
sudo apt -y upgrade

    # If this is a Hyper-V guest:
#sudo apt-get -y install linux-azure



################################################################################
### Configure some user-specific things
################################################################################

    ### Aliases I find useful
echo "alias upgrayedd='sudo apt update && sudo apt -y full-upgrade && sudo apt -y autoremove'" >>  ~/.bash_aliases
echo "alias ll='ls -alFh'" >>  ~/.bash_aliases
echo "alias cls='clear && ll'" >>  ~/.bash_aliases


    # upload .ssh folder and set permissions if needed, or generate new key
#ssh-keygen -f .ssh/id_rsa -t rsa -N ''

    # If uploaded - ensure permissions set correctly
#chmod 700 .ssh/
#chmod 600 .ssh/id_rsa
#chmod 644 .ssh/id_rsa.pub


    # Non-persistent environment variables
UW_EMAIL=`whoami`'@uw.edu'
USER_FULLNAME=`getent passwd $UID | awk -F ":" '{print $5}'`

    # tell git who you are
git config --global user.email "$UW_EMAIL"
git config --global user.name "$USER_FULLNAME"



################################################################################
# configure webdev environment
################################################################################

    # Install Python
sudo apt-get -y install python3 python3-pip python3-bs4

    # Configure Python - set python3 as the default and make a symlink so 'pip' points to 'pip3'
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1
sudo ln -s /usr/bin/pip3 /usr/bin/pip


    # Install Apache
sudo apt-get -y install apache2 libapache2-mod-wsgi-py3

    # Permissions structure - so you don't need sudo to do webdev once the server is configured
sudo groupadd webadmin
sudo usermod -a -G webadmin $USER  #won't take effect until next login session
sudo chown -R :webadmin /var/www
sudo chmod -R g+ws /var/www        #set guid - means child files/directories will inherit group permissions
ln -s /var/www ~/www

    # disable multithreading processes
sudo a2dismod mpm_event

    # Enable various apache modules
sudo a2enmod mpm_prefork cgi ssl rewrite wsgi

    # Disable the auto-startup of Apache.
    # Don't run this if you're using apache as your host
sudo systemctl disable apache2

    # makes sure it runs on startup. use this if you ARE using apache as your host
#sudo systemctl enable apache2


    # MySQL is a standard component for most web hosting
    # Run these if you are planning on using it
#sudo apt-get -y install mysql-server
#sudo -i mysql_secure_installation


################################################################################
### Install project-specific components
################################################################################


    ### displaCy REST microservices
    ### forked from https://github.com/explosion/spacy-services
    ### github => bitbucket forking: https://paazmaya.fi/forking-a-repository-from-github-to-bitbucket
git clone git@bitbucket.org:justfoo/spacy-services.git /var/www/spacy-services
sudo -H pip install -r /var/www/spacy-services/uw/requirements.txt


    ### daemonize the REST service http://devopspy.com/linux/python-script-linux-systemd-service/
    ### Create the unit file
echo -e "[Unit]
Description=SpaCy REST API services
After=multi-user.target

[Service]
Type=idle
ExecStart=/usr/bin/python /var/www/spacy-services/uw/app.py

[Install]
WantedBy=multi-user.target" | sudo tee -a /lib/systemd/system/spacyapi.service


    ### set permissions, Reload the systemd, and enable service on system bootup
sudo chmod 644 /lib/systemd/system/spacyapi.service
sudo systemctl daemon-reload
sudo systemctl enable spacyapi.service


    ### to check status
#service spacyapi status

    ### Reboot
sudo reboot now

